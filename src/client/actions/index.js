import axios from 'axios';

export const FETCH_TRAITS = 'fetch_traits';
export const ADD_NEW_USER = 'add_new_user';
export const ADD_TRAIT_TO_USER = 'add_trait_to_user';
export const FETCH_USER = 'fetch_user';

const ROOT_URL = 'http://localhost:9000';

export function fetchTraits() {
    const request = axios.get(`${ROOT_URL}/api/user/withTrait/FooBar`);

    return {
        type: FETCH_TRAITS,
        payload: request
    }
}
export function fetchUser(user_id) {
    const request = axios.get(`${ROOT_URL}/api/user/getUser/${user_id}`);

    return {
        type: FETCH_USER,
        payload: request
    }
}

export function addNewUser(values, callback) {
    const request = axios.post(`${ROOT_URL}/api/user/newUser`, values)
        .then(response => callback(response));

    return{
        type: ADD_NEW_USER,
        payload: request
    }
}
export function addTraitScoreToUser(values, user_id, callback) {
    // console.log("values in addTraitScoreToUser", values);
    const request = axios.post(`${ROOT_URL}/api/traitScore/newTrait/${user_id}`, values)
        .then(() => callback());

    return{
        type: ADD_TRAIT_TO_USER,
        payload: request
    }
}