import React, { Component } from 'react';
import {Field, reduxForm} from 'redux-form';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {addTraitScoreToUser, fetchTraits, fetchUser} from '../actions'
import '../scss/application.scss';


class AddTraitScoreToUser extends Component {

    componentDidMount(){
        const {_id} = this.props;
        // console.log("_id in CDM", _id);
        this.props.fetchUser(_id);
    }

    renderTraitField(field){
        const {meta: {touched, error}} = field;
        const className = `form-group ${touched && error}`;

        return(
            <div className={className}>
                <label>{field.label}:</label>
                <input
                    type="text"
                    className="App-input-text"
                    {...field.input}
                />
                {touched ? <div className="App-error App-trait-error">{error}</div> : ""}
            </div>
        )
    }
    renderScoreField(field){
        return(
            <div className="form-group App-score">
                <label className="App-score-label">{field.label}:</label>
                <input
                    type="number"
                    className="App-input-text"
                    {...field.input}
                />
            </div>
        )
    }
    renderIsValidField(field){
        return(
            <div className="form-group">
                <label>{field.label}:</label>
                <input
                    type="checkbox"
                    className="App-input-checkbox"
                    {...field.input}
                />
            </div>
        )
    }

    onSubmit(values){
        const {_id} = this.props;
        // console.log(values)
        this.props.addTraitScoreToUser({...values, ["AppUser"]:[_id]}, _id, () => {
            this.props.fetchTraits();
            this.props.history.push('/app-assign3')
        });
    }

    render(){
        const {handleSubmit} = this.props;
        const {user} = this.props;
        // console.log("user in render", user);
        if(!user){
            return <div>Loading....</div>
        }

        return(
            <div className="App-tech-body">
                <div className="App-sub-section">
                    <h1 className="App-sub-title">Add Trait to User</h1>
                    <p className="App-intro">{user.Name}</p>
                </div>
                <div className="App-demo-tech">
                    <form className="App-form" onSubmit={handleSubmit(this.onSubmit.bind(this))}>

                        <Field
                            label="Trait"
                            name="Trait"
                            value="FooBar"
                            component={this.renderTraitField}
                        />
                        <Field
                            label="Score"
                            name="Score"
                            component={this.renderScoreField}
                        />
                        <Field
                            label="Is Valid"
                            name="isValid"
                            component={this.renderIsValidField}
                        />
                        <button className="App-submit-btn" type="submit">Add</button>
                        <div className="App-cancel">
                            <Link to="/app-assign3" className="">Cancel</Link>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

function mapStateToProps({user}, ownProps) {
    // console.log("fetchUser in MSTP = ", user);
    return {
        user: user.user,
        _id: ownProps.match.params._id
    }
}

function validate(values){
    const errors = {

    };

    if(values.Trait != "FooBar"){
        errors.Trait = "Trait is required to be FooBar for this demo";
    }

    return errors
}

export default reduxForm({
    validate,
    form: 'NewUserForm'
})(
    connect(mapStateToProps, {fetchUser, addTraitScoreToUser, fetchTraits})(AddTraitScoreToUser)
);