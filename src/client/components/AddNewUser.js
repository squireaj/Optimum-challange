import React, { Component } from 'react';
import {postUser} from "../actions";
import {Field, reduxForm} from 'redux-form';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {addNewUser} from '../actions'
import '../scss/application.scss';


class AddNewUser extends Component {
    renderNameField(field){
    const {meta: {touched, error}} = field;
    const className = `form-group ${touched && error}`;

        return(
            <div className={className}>
                <label>{field.label}:</label>
                <input
                    type="text"
                    className="App-input-text"
                    {...field.input}
                />
                {touched ? <div className="App-error">{error}</div> : ""}
            </div>
        )
    }
    renderIsActiveField(field){
        return(
            <div className="form-group">
                <label>{field.label}:</label>
                <input
                    className="App-input-checkbox"
                    type="checkbox"
                    {...field.input}
                />
            </div>
        )
    }

    onSubmit(values){
        // console.log(values)
        this.props.addNewUser(values, response => {
            const _id = response.data._id;
            console.log("_id", _id);
            this.props.history.push(`/app-addtraitscore/${_id}`)
        });
    }

    render(){
        const {handleSubmit} = this.props;

        return(
            <div className="App-tech-body">
                <div className="App-sub-section">
                    <h1 className="App-sub-title">New User</h1>
                    <p className="App-intro">Add a New User</p>
                </div>
                <div className="App-demo-tech">
                    <form className="App-form" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                        <Field
                            label="Name"
                            name="Name"
                            component={this.renderNameField}
                        />
                        <Field
                            label="Is Active"
                            name="isActive"
                            component={this.renderIsActiveField}
                        />
                        <button className="App-submit-btn" type="submit">Add</button>
                        <div className="App-cancel">
                            <Link to="/app-assign3" className="">Cancel</Link>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

function validate(values){
    const errors = {

    };

    if(!values.Name){
        errors.Name = "Name is required";
    }

    return errors
}

export default reduxForm({
    validate,
    form: 'NewUserForm'
})(
    connect(null, {addNewUser})(AddNewUser)
);