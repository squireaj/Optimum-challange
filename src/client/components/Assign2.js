import React, { Component } from 'react';
import '../scss/application.scss';

function Assign2 (){
        return(

            <div className="App-tech-body">
                <div className="App-sub-section">
                    <h1 className="App-sub-title">Recommended Technologies</h1>
                    <p className="App-intro">For Opti-Corp’s prototype I would recommend the following technologies:</p>
                </div>
                <div className="App-demo-tech">
                    {/*<h3 className="App-tech-section-header">For Opti-Corp’s prototype I would recommend the following technologies:</h3>*/}
                    <h5 className="App-tech-header">View - React/Redux:</h5>
                    <ul>
                        <li className="App-tech-explanation">React accompanied by Redux is a good choice because it does a great job of managing the complex profile and document state in our application. (I’m not sure if documents are created dynamically or uploaded)</li>
                        <li className="App-tech-explanation">Redux Form is a useful library that can be used for quick and easy form validation</li>
                        <li className="App-tech-explanation">React-chartjs is a good example of pre-made react grid and chart components that could quickly be plugged in for a prototype</li>
                    </ul>
                    <h5 className="App-tech-header">Server - Node:</h5>
                    <ul>
                        <li className="App-tech-explanation">Node is a tool that could easily expose API’s for internal as well as external consumption</li>
                    </ul>
                    <h5 className="App-tech-header">Database - Mongo/Express:</h5>
                    <ul>
                        <li className="App-tech-explanation">As a noSQL database mongo would be able to store documents who’s shape is not yet known and so do not have a pre defined schema</li>
                        <li className="App-tech-explanation">Express provides methods that make writing complex queries quicker and easier</li>
                    </ul>
                </div>
            </div>
            )
}

export default Assign2