import React, { Component } from 'react';
import {connect} from 'react-redux';
import {fetchTraits} from "../actions";
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route, Switch, Link} from 'react-router-dom';
import _ from 'lodash';
import '../scss/application.scss';
import AddNewUser from "./AddNewUser";
import reducers from "../reducers";
import promise from "redux-promise";

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

class Assign3 extends Component{

    componentDidMount(){
        this.props.fetchTraits();
    }

    formatDate(date) {
        let newDate = new Date(date);
        let hours = newDate.getHours();
        let minutes = newDate.getMinutes();
        let ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        let strTime = hours + ':' + minutes + ' ' + ampm;
        return newDate.getMonth()+1 + "/" + newDate.getDate() + "/" + newDate.getFullYear() + "  " + strTime;
    }

    renderTraits(){
        // console.log("Traits in renderTraits in assign3 =", this.props.traits);
        const {traits} = this.props.traits;
        return _.map(traits, trait => {
            return (
                <div className="App-user-results" key={trait.traitId}>
                    <h3 className="App-user-name">{trait._id}</h3>
                            <div className="App-user-value">
                                <h5 className="normalize"><span className="label">Trait:</span> {trait.traitname}</h5>
                            </div>
                            <div className="App-user-value">
                                <h5 className="normalize"><span className="label">Score:</span> {trait.traitScore}</h5>
                            </div>
                            <div className="App-user-value">
                                <h5 className="normalize"><span className="label">Date Created:</span> {this.formatDate(trait.CreatedDate)}</h5>
                            </div>
                            <div className="App-user-value">
                                <h5 className="normalize"><span className="label">Is Active:</span> {trait.isActive.toString()}</h5>
                            </div>

                </div>
            )
        })
    }

    render(){
        const {traits} = this.props.traits;
        // console.log("this.props.traits =", this.props.traits);
        if(_.isEmpty(traits) || Object.keys(traits)[0] == "user"){
            return(
                <div className="App-tech-body">
                    <div className="App-sub-section">
                        <h1 className="App-sub-title">Data Organization</h1>
                        <p className="App-intro">Please See Readme To Add Data</p>
                        <Link to="/app-adduser" className="App-button">New User</Link>
                        <p className="App-warning">*Query is only looking for the trait "FooBar"</p>

                        <Provider store={createStoreWithMiddleware(reducers)}>
                            <div className="App">
                                <BrowserRouter>
                                    <div>
                                        <Switch>
                                            <Route path="/assign3/addUser" component={AddNewUser} />
                                        </Switch>
                                    </div>
                                </BrowserRouter>
                            </div>
                        </Provider>
                    </div>
                </div>
            )
        }
        return(
            <div className="App-tech-body">
                <div className="App-sub-section">
                    <h1 className="App-sub-title">Data Organization</h1>
                    <p className="App-intro">See Query Results Below</p>
                    <Link to="/app-adduser" className="App-button">New User</Link>
                </div>
                <div className="App-demo-tech">
                      {this.renderTraits()}
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {traits: state.traits}
}

export default connect(mapStateToProps, {fetchTraits})(Assign3);