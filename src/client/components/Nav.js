import React, {Component} from 'react';
import {Link} from 'react-router-dom';


class Nav extends Component{

    constructor(props) {
        super(props);
        this.state = {
            activeTab: "assignment1"
        };
    }
    changeActiveTab(activeTab) {
        return this.setState({activeTab});
    };

    render(){
        const {activeTab} = this.state
        return(
        <div>
            <header className="App-header">
                <div className="App-header-left">
                    <div className="App-title">Squire's</div>
                    <div className="App-title">Submission</div>
                </div>
                <div className="App-header-right">
                    <Link to="/" className={activeTab == "assignment1" ? "App-tab App-tab-active" : "App-tab"} onClick={()  => this.changeActiveTab("assignment1")}>ASSIGNMENT 1</Link>
                    <Link to="/app-assign2" className={activeTab == "assignment2" ? "App-tab App-tab-active" : "App-tab"} onClick={()  => this.changeActiveTab("assignment2")}>ASSIGNMENT 2</Link>
                    <Link to="/app-assign3" className={activeTab == "assignment3" ? "App-tab App-tab-active" : "App-tab"} onClick={()  => this.changeActiveTab("assignment3")}>ASSIGNMENT 3</Link>
                </div>
            </header>
            <div className="horizontal-line"/>
        </div>
        )
    }

}

export default Nav