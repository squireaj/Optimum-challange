
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route, Switch, Link} from 'react-router-dom';
import promise from 'redux-promise';
import './scss/application.scss';
import reducers from './reducers/index';
import Nav from './components/Nav.js';
import Assign1 from './components/Assign1.js';
import Assign2 from './components/Assign2.js';
import Assign3 from './components/Assign3.js';
import AddNewUser from './components/AddNewUser.js';
import AddNewTraitScore from './components/AddNewTraitScore.js';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);


ReactDOM.render(
    <Provider store={createStoreWithMiddleware(reducers)}>
        <div className="App">
            <BrowserRouter>
                <div>
                    <Nav />
                    <Switch>
                        <Route path="/" exact component={Assign1} />
                        <Route path="/app-assign2" component={Assign2} />
                        <Route path="/app-assign3" exact component={Assign3} />
                        <Route path="/app-adduser" component={AddNewUser} />
                        <Route path="/app-addtraitscore/:_id" component={AddNewTraitScore} />
                    </Switch>
                </div>
            </BrowserRouter>
            </div>
    </Provider>
    , document.getElementById("root"));

