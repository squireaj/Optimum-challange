import { combineReducers } from 'redux';
import {reducer as formReducer} from 'redux-form'
import TraitsReducer from './reducer_traits';
import UserReducer from './reducer_traits';
import AddedTraitUser from './reducer_traits';

const rootReducer = combineReducers({
    addedUser: AddedTraitUser,
    user: UserReducer,
    traits: TraitsReducer,
    form: formReducer
});

export default rootReducer;