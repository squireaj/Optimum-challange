import {ADD_TRAIT_TO_USER, FETCH_TRAITS, FETCH_USER} from "../actions";
import _ from 'lodash';


export default function (state = {}, action) {
    switch (action.type){
        case FETCH_TRAITS:
            return Object.assign({}, state,{
                traits: _.mapKeys(action.payload.data, "_id")
            });
        case FETCH_USER:
            return Object.assign({}, state, {
                user: action.payload.data
            });
        case ADD_TRAIT_TO_USER:
            return state;
        default:
            return state
    }
}