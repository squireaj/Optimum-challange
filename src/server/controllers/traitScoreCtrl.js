
const TraitScore     = require('../models/traitScoreModel.js'),
      User           = require('../models/userModel'),
      express        = require('express'),
      mongoose       = require('mongoose'),
      traitScoreCtrl = express.Router();


traitScoreCtrl.post('/newTrait/:user_id', createTraitScore);

module.exports = traitScoreCtrl;

    function createTraitScore(req, res){
        let newTraitScore = new TraitScore(req.body);
        newTraitScore.save(function(err, newTraitScore) {
            if (err) {
                console.log(err);
                return res.status(500).json(err);
            }
            // return res.json(newTraitScore);
        }).then(newTraitScore => {
           try{
               pushTraitScoreToUser(newTraitScore._id, req, res)
           }
           catch(err){
               return res.status(500).json(err);
           }
        })
    }

    function pushTraitScoreToUser(traitScore_Id,req, res) {
        User.findByIdAndUpdate(req.params.user_id, {$push:{TraitScores: traitScore_Id}}, {new: true}, function(err, user) {
            if (err) {
                return res.status(500).json(err);
                // console.log("error in adding traitScore to user")
            }
            res.status(200).send(user)
                // console.log("attached traitscore to user")
        })};