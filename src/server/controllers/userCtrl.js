
const User       = require('../models/userModel.js'),
      TraitScore = require('../models/traitScoreModel.js'),
      express    = require('express'),
      mongoose   = require('mongoose'),
      userCtrl   = express.Router();

userCtrl.post('/newUser', createUser);
userCtrl.get('/getUser/:_id', getUser);
userCtrl.get('/withTrait/:trait', getUsersWithTrait);


module.exports = userCtrl;

    function createUser(req, res) {
        // console.log("create users api hit");
        User.findOne({ Name: req.body.Name })
            .then( user => {

                // - if we found a user, it's a duplicate

                if (user) {
                    return res.status(400).json({message: "User with this name already exists."});
                }
                // - otherwise, create the user
                var user = new User(req.body);
                // console.log("creating new user");
                user.save(function(err, new_user) {
                    if (err) {
                        console.log("can't create user", err);
                    }
                    res.json(new_user);
                })
            })
    };

    function getUsersWithTrait(req, res) {
        // console.log("trait= ", req.params.trait);
        TraitScore
            .aggregate([
                {$match: {$and: [{Trait: req.params.trait}, {IsValid: true}, {Score: {$gt: 3}}]}},
                {$lookup:{from: 'users', localField: 'AppUser', foreignField: '_id', as: 'User'}},
                {$sort: {CreatedDate: -1}},
                {$unwind: '$User'},
                {$group:{
                        _id              : '$User.Name',
                        traitId          : {$first: '$_id'},
                        traitname        : {$first: '$Trait'},
                        traitCreatedDate : {$first: '$CreatedDate'},
                        traitScore       : {$first: '$Score'},
                        isValid          : {$first: '$IsValid'},
                        isActive         : {$first: '$User.IsActive'},
                        CreatedDate      : {$first: '$User.CreatedDate'}
                }},
                {$sort: {traitScore: -1}}
            ])
            .exec(function (err, traits) {
                if(err){
                    console.log(err);
                    return res.status(500).json(err);
                }
                return res.json(traits);
            })
    };

 function getUser(req, res) {
     // console.log("fetching User");
    User.findById(req.params._id)
        .exec().then(function(User) {
        if (!User) {
            return res.status(404).end();
        }
        return res.json(User);
    });
};