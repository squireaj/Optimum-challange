
const express        = require('express'),
    bodyParser     = require('body-parser'),
    mongoose       = require('mongoose'),
    mongoURI       = 'mongodb://localhost:27017/trait-score',
    userCtrl       = require('./controllers/userCtrl'),
    traitScoreCtrl = require('./controllers/traitScoreCtrl'),
    path           = require('path'),
    app            = express(),
    port           = 9000;

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname+"./../../")));
mongoose.connect(mongoURI, { useMongoClient: true });


app.listen(port, () => {console.log(`Listening on port: ${port}`)});

//Create and app user


app.use('/api/user', userCtrl);
app.use('/api/traitScore', traitScoreCtrl);


app.get(/(app)/ , function (request, response){
    response.sendFile(path.join(__dirname+"./../../", 'index.html'))
});
